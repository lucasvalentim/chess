from chess.constants import *
from chess import Chess

if __name__ == '__main__':
    game = Chess()

    game.draw_board()

    try:
        while not game.in_checkmate():
            movement = input('Digite o movimento: ')

            if not game.move(movement):
                if game.in_check():
                    print('\nMovimento inválido, pois ele não tira o rei de xeque.')

                else:
                    print('\nMovimento inválido.')

            game.draw_board()

            if game.in_checkmate():
                if game.turn == WHITE:
                    print('Jogo finalizado com xeque-mate! Vitória das pretas.\n')

                else:
                    print('Jogo finalizado com xeque-mate! Vitória das brancas.\n')

    except KeyboardInterrupt:
        print('\n\nPartida abandonada pelo jogador.\n')
